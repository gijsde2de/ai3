# mira.py
# -------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


# Mira implementation
import util
PRINT = True

class MiraClassifier:
    """
    Mira classifier.

    Note that the variable 'datum' in this code refers to a counter of features
    (not to a raw samples.Datum).
    """
    def __init__( self, legalLabels, max_iterations):
        self.legalLabels = legalLabels
        self.type = "mira"
        self.automaticTuning = False
        self.C = 0.001
        self.legalLabels = legalLabels
        self.max_iterations = max_iterations
        self.initializeWeightsToZero()

    def initializeWeightsToZero(self):
        "Resets the weights of each label to zero vectors"
        self.weights = {}
        for label in self.legalLabels:
            self.weights[label] = util.Counter() # this is the data-structure you should use

    def train(self, trainingData, trainingLabels, validationData, validationLabels):
        "Outside shell to call your method. Do not modify this method."

        self.features = trainingData[0].keys() # this could be useful for your code later...

        if (self.automaticTuning):
            Cgrid = [0.002, 0.004, 0.008]
        else:
            Cgrid = [self.C]

        return self.trainAndTune(trainingData, trainingLabels, validationData, validationLabels, Cgrid)

    def trainAndTune(self, trainingData, trainingLabels, validationData, validationLabels, Cgrid):
        """
        This method sets self.weights using MIRA.  Train the classifier for each value of C in Cgrid,
        then store the weights that give the best accuracy on the validationData.

        Use the provided self.weights[label] data structure so that
        the classify method works correctly. Also, recall that a
        datum is a counter from features to values for those features
        representing a vector of values.
        """
        "*** YOUR CODE HERE ***"
        testWeights = util.Counter()
        
        # Learn the test data for every C in Cgrid
        for c in Cgrid:
            # Separate weights for every C
            testWeights[c] = self.weights.copy()
            # Learning is almost same as perceptron learning.
            for iteration in range(self.max_iterations):
                print "Starting iteration ", iteration, "..."
                for i in range(len(trainingData)):
                    # calculate the scores of each label
                    score = util.Counter()
                    for label in self.legalLabels:
                        score[label] = trainingData[i] * testWeights[c][label]
                    
                    # the label we think it is
                    label = score.argMax()
                    
                    # if the label is the correct label, do nothing. 
                    # else apply formula
                    if trainingLabels[i] != label:
                    
                        # Make the square of labels, (not the dot product)
                        square = util.Counter()
                        for value in trainingData[i]:
                            square[value] = trainingData[i][value] * trainingData[i][value]

                        # Calculate formula given in exercise
                        result = ((testWeights[c][label] - testWeights[c][trainingLabels[i]]) * trainingData[i] + 1.0) / (2 * square.totalCount())
                        result = min(c, result)
                        
                        # Multiply result of formula with label and add that to the weights.
                        newFeature = trainingData[i].copy()
                        
                        for feature in newFeature:
                            newFeature[feature] *= result
                         
                        testWeights[c][trainingLabels[i]] += newFeature  
                        testWeights[c][label] -= newFeature
                        
        # Now to figure out the best C value  
        # Classify the validationData for each CODE
        # Keep track how many are correct and select C with highest score.
        highscore = 0
        for c in Cgrid:
            score = 0
            # Set weights temporarily to get correct answers from classify
            self.weights = testWeights[c]
            answers = self.classify(validationData)
            
            # Check answers, and add score
            counter = 0
            for answer in answers:
                if answer == validationLabels[counter]:
                    score += 1
                counter += 1
                
            # if score > highscore set new C to best c
            if score > highscore:
                highscore = score
                highscoreC = c
            # in the exercise is mentioned if result is equal, lower c wins
            if score == highscore:
                highscoreC = min(c, highscoreC)
                
        # set weights to best c weightsS
        self.weights = testWeights[highscoreC]                                               
                    

    def classify(self, data ):
        """
        Classifies each datum as the label that most closely matches the prototype vector
        for that label.  See the project description for details.

        Recall that a datum is a util.counter...
        """
        guesses = []
        for datum in data:
            vectors = util.Counter()
            for l in self.legalLabels:
                vectors[l] = self.weights[l] * datum
            guesses.append(vectors.argMax())
        return guesses


